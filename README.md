# Harbour

Script to integrate SeaBIOS with existing Libreboot ROM images. Based on the instructions provided [here](https://notabug.org/libreboot/libreboot/issues/467).

~~Currently works only with the Thinkpad X200, T400 and T500.~~ 

Works with all supported Libreboot devices. Tested successfully with an 8MB X200, and an X60s.

Use with caution! Always have a verified working backup of your current ROM, and a way to restore it.

##TODO

* ~~Better-formatted, more descriptive variable names.~~
* Ensure exit status of Libreboot scripts.
* ~~Replace hardcoded Libreboot version with detection of latest stable.~~