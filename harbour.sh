#!/bin/bash
##Find latest LibreBoot release
wget --no-remove-listing https://mirror.math.princeton.edu/pub/libreboot/stable/
LatestStable="$(grep -Eo '[0-9]+' index.html | sort -rn | head -n 1)"
rm index.html

##Download and extract SeaVGABIOS from existing LibreBoot image.
wget https://mirror.math.princeton.edu/pub/libreboot/stable/"$LatestStable"/rom/seabios/libreboot_r"$LatestStable"_seabios_d945gclf.tar.xz
wget https://mirror.math.princeton.edu/pub/libreboot/stable/"$LatestStable"/libreboot_r"$LatestStable"_util.tar.xz
tar xf libreboot_r"$LatestStable"_seabios_d945gclf.tar.xz
tar xf libreboot_r"$LatestStable"_util.tar.xz
arch="$(uname -m)"
cp -r libreboot_r"$LatestStable"_util/cbfstool/"$arch"/cbfstool cbfstool
./cbfstool libreboot_r"$LatestStable"_seabios_d945gclf/d945gclf_txtmode.rom extract -n vgaroms/vgabios.bin -f vgabios.bin
rm -rf libreboot*

##Generate device list, select device and download image.
DeviceList=($(wget --no-remove-listing https://mirror.math.princeton.edu/pub/libreboot/stable/"$LatestStable"/rom/grub/ ; grep -o -P '(?<=href="libreboot_r20160907_grub_).*(?=.tar.xz")' index.html))
DeviceList=($(printf "%s\n" "${DeviceList[@]}" | sort -u))
DeviceCheck=0
while [ "$DeviceCheck" == 0 ]
do
	echo "Please select your device:"
	for DeviceCounter in ${!DeviceList[*]}
	do
		printf "%4d: %s\n" $DeviceCounter ${DeviceList[$DeviceCounter]}
		DeviceCounter=$(( $DeviceCounter + 1 ))
	done
	read -r SelectedDevice
	if (("$SelectedDevice" >= 0 )) && (( "$SelectedDevice" <= "$DeviceCounter" ))
	then
		DeviceCheck=1
		else
			echo "Invalid choice."
			echo ""
	fi
done
Device=${DeviceList[SelectedDevice]}
echo ""
echo "Downloading for $Device"
wget https://mirror.math.princeton.edu/pub/libreboot/stable/"$LatestStable"/rom/grub/libreboot_r"$LatestStable"_grub_"$Device".tar.xz
tar -xf libreboot_r"$LatestStable"_grub_"$Device".tar.xz

##Select language/layout and framebuffer 
LangList=($(ls libreboot_r"$LatestStable"_grub_"$Device"/"$Device"_* | awk -F _ '{print $(NF-1)}'))
LangList=($(printf "%s\n" "${LangList[@]}" | sort -u))
LangCheck=0
while [ "$LangCheck" == 0 ]
do
	echo "Please select your language/layout:"
	for LangCounter in ${!LangList[*]}
	do
		printf "%4d: %s\n" $LangCounter ${LangList[$LangCounter]}
		LangCounter=$(( $LangCounter + 1 ))
	done
	read -r SelectedLang
	if (("$SelectedLang" >= 0 )) && (( "$SelectedLang" <= "$LangCounter" ))
	then
		LangCheck=1
		else
			echo "Invalid choice."
			echo ""
	fi
done
Lang=${LangList[SelectedLang]}
echo ""

BufferList=($(basename -a -s .rom libreboot_r"$LatestStable"_grub_"$Device"/"$Device"_* | awk -F _ '{print $(NF)}'))
BufferList=($(printf "%s\n" "${BufferList[@]}" | sort -u))
BufferCheck=0
while  [ "$BufferCheck" == 0 ]
do
	echo "Please select your framebuffer:"
	for BufferCounter in ${!BufferList[*]}
	do
		printf "%4d: %s\n" $BufferCounter ${BufferList[$BufferCounter]}
		BufferCounter=$(( $BufferCounter + 1 ))
	done
	read -r SelectedBuffer
	if (("$SelectedBuffer" >= 0 )) && (( "$SelectedBuffer" <= "$BufferCounter" ))
	then
		BufferCheck=1
		else
			echo "Invalid choice."
			echo ""
	fi
done
Buffer=${BufferList[SelectedBuffer]}
echo ""

Filename=""$Device"_"$Lang"_"$Buffer".rom"

cp -r libreboot_r"$LatestStable"_grub_"$Device"/$Filename $Filename
rm -rf libreboot*

##Integrate SeaVGABIOS.
echo "Inegrating SeaBIOS."
./cbfstool $Filename add  -f vgabios.bin -n vgaroms/vgabios.bin -t raw
./cbfstool $Filename remove -n bootorder
./cbfstool $Filename remove -n etc/show-boot-menu
echo "Done!"

##Tidy up directory
mv ./*.rom SeaBIOS_$Filename
rm cbfstool
rm vgabios.bin
rm index.html
exit
